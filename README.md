# z0s/Cache

### Installation
You can include it in your project using: `composer require z0s/cache`

### Requirements
1. PHP8.0 or higher
2. Redis
3. ext-redis
4. ext-igbinary

### Usage

```php

$connection = new \z0s\Cache\Connection([
    'host' => [
        'tcp://127.0.0.1'
    ],
    'options' => [
        //'replication' => 'sentinel',
        //'service' => 'z0s',
        //'autodiscovery' => true,
        //'cluster' => 'predis',
        'parameters' => [
            // 'password' => ''
        ]
    ]
);

$cache = new \z0s\Cache\Cache($connection);

// Add data
$cache->set('key', 'value', 3600);

// Get data
$cache->get('key');

// Delete
$cache->delete('key');
```
